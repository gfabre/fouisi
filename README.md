THEME_fouisi


Thème graphique moodle pour le projet OuiSi, basé sur la charte graphique MTU d'Unisciel (cf. https://github.com/stephanepoinsart/moodle-theme_fordson ) , utilisant le thème Fordson. Le thème fordson a du être modifié car Moodle ne peut faire coexister 2 thèmes identiques.

===========
Actions réalisées :

1) Import de https://github.com/stephanepoinsart/moodle-theme_fordson via https://framagit.org/import/github/status
2) Se mettre dans le dossier FORDSON-OUISI, ouvrir un terminal et entrer git clone https://framagit.org/gfabre/fouisi
cd fouisi

Configurer ses accès :

git config --global user.name "Ghislain Fabre"

couper le .git et le mettre en amont de ce dossier

appliquer les changements de nom pour permettre au thème de coexister avec un autre thème (cf.  cf. doc de Stéphane : https://framagit.org/stephanep/moodle-theme_focus/-/blob/master/rebase.txt soit en le personalisant :
 ) :

find -name "*fordson*" -exec rename 's/fordson/fouisi/' {} ";"
find . -type f  -exec sed -i'' -e 's/fordson/fouisi/g' {} \;
find . -type f  -exec sed -i'' -e 's/Fordson/fouisi/g' {} \;
find . -type f  -exec sed -i'' -e 's/FORDSON/fouisi/g' {} \;
find . -type f  -exec sed -i'' -e 's/page_location_incourse_themeconfig/fouisi_page_location_incourse_themeconfig/g' {} \;

coller .git où il était.

git status 


git add lang/en/theme_fouisi.php
git add lib/fouisi_lib.php
git add scss/fouisi_variables.scss
git commit -a

===========

Guide de survie de Stéphane

Récupérer une premiere fois :

git clone https://github.com/stephanepoinsart/moodle-theme_fordson.git

Aller dans le répertoire en question...


Configurer ses accès (par exemple) :

git config --global user.name "Ghislain Fabre"
git config --global user.email a@a.com


Récupérer les mises à jours par la suite :

git pull


Faire des changements sur les fichiers...


Voir les fichiers changés en local :

git status


Commiter son travail en local :

git add mes-fichiers-modifiers

git commit


Envoyer sur le serveur github :

git push




===========

# fouisi

fouisi is focused on students going from login to learning, with features that help teachers build better courses and students engage with content. Your school is unique and fouisi provides impressive customizations for a professional and modern learning platform. 

# Install from Github
Click on the button to "Clone or Download" https://github.com/dbnschools/moodle-theme_fouisi . When downloaded to your computer, unzip it. It should create a folder named "moodle-theme_fouisi-master". Rename the folder so that it is "fouisi" (without quotes). You can FTP that folder to your moodle site in /moodle/theme/ directory. Or you can create a new ZIP file of the "fouisi" folder and upload and install it via the Plugin Administration in Site Administration.


# Versions and Updates

## Moodle 3.7 fouisi v3.7 release 1.1
* Fixed missing quote https://github.com/dbnschools/moodle-theme_fouisi/issues/74
* Fixed course completion bar showing when not logged in https://github.com/dbnschools/moodle-theme_fouisi/issues/73

## Moodle 3.7 fouisi v3.7 release 1
* Initial release for Moodle 3.7