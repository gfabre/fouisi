<?php

defined('MOODLE_INTERNAL') || die();

/* Marketing Spot Settings temp*/
$page = new admin_settingpage('theme_fouisi_marketing', get_string('marketingheading', 'theme_fouisi'));

// Toggle FP Textbox Spots.
$name = 'theme_fouisi/togglemarketing';
$title = get_string('togglemarketing' , 'theme_fouisi');
$description = get_string('togglemarketing_desc', 'theme_fouisi');
$displaytop = get_string('displaytop', 'theme_fouisi');
$displaybottom = get_string('displaybottom', 'theme_fouisi');
$default = '2';
$choices = array('1'=>$displaytop, '2'=>$displaybottom);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot One
$name = 'theme_fouisi/marketing1info';
$heading = get_string('marketing1', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot One
$name = 'theme_fouisi/marketing1';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing1image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing1image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing1content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing1buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing1buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing1target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Two
$name = 'theme_fouisi/marketing2info';
$heading = get_string('marketing2', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Two.
$name = 'theme_fouisi/marketing2';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing2image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing2image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing2content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing2buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing2buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing2target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Three
$name = 'theme_fouisi/marketing3info';
$heading = get_string('marketing3', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Three.
$name = 'theme_fouisi/marketing3';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing3image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing3image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing3content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing3buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing3buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing3target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_fouisi/marketing4info';
$heading = get_string('marketing4', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_fouisi/marketing4';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing4image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing4image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing4content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing4buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing4buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing4target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_fouisi/marketing5info';
$heading = get_string('marketing5', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_fouisi/marketing5';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing5image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing5image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing5content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing5buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing5buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing5target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot Four
$name = 'theme_fouisi/marketing6info';
$heading = get_string('marketing6', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot
$name = 'theme_fouisi/marketing6';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing6image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing6image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing6content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing6buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing6buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing6target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_fouisi/marketing7info';
$heading = get_string('marketing7', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Seven
$name = 'theme_fouisi/marketing7';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing7image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing7image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing7content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing7buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing7buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing7target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_fouisi/marketing8info';
$heading = get_string('marketing8', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Eight
$name = 'theme_fouisi/marketing8';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing8image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing8image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing8content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing8buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing8buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing8target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// This is the descriptor for Marketing Spot
$name = 'theme_fouisi/marketing9info';
$heading = get_string('marketing9', 'theme_fouisi');
$information = get_string('marketinginfodesc', 'theme_fouisi');
$setting = new admin_setting_heading($name, $heading, $information);
$page->add($setting);

// Marketing Spot Nine
$name = 'theme_fouisi/marketing9';
$title = get_string('marketingtitle', 'theme_fouisi');
$description = get_string('marketingtitledesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Background image setting.
$name = 'theme_fouisi/marketing9image';
$title = get_string('marketingimage', 'theme_fouisi');
$description = get_string('marketingimage_desc', 'theme_fouisi');
$setting = new admin_setting_configstoredfile($name, $title, $description, 'marketing9image');
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing9content';
$title = get_string('marketingcontent', 'theme_fouisi');
$description = get_string('marketingcontentdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing9buttontext';
$title = get_string('marketingbuttontext', 'theme_fouisi');
$description = get_string('marketingbuttontextdesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing9buttonurl';
$title = get_string('marketingbuttonurl', 'theme_fouisi');
$description = get_string('marketingbuttonurldesc', 'theme_fouisi');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

$name = 'theme_fouisi/marketing9target';
$title = get_string('marketingurltarget' , 'theme_fouisi');
$description = get_string('marketingurltargetdesc', 'theme_fouisi');
$target1 = get_string('marketingurltargetself', 'theme_fouisi');
$target2 = get_string('marketingurltargetnew', 'theme_fouisi');
$target3 = get_string('marketingurltargetparent', 'theme_fouisi');
$default = 'target1';
$choices = array('_self'=>$target1, '_blank'=>$target2, '_parent'=>$target3);
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$page->add($setting);

// Must add the page after definiting all the settings!
$settings->add($page);